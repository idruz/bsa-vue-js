import { ADD_CURRENCY, FETCH_CURRENCY } from "./types/actions";
import { SET_CURRENCY } from "./types/mutations";

export function getActions() {
    return {
        [FETCH_CURRENCY]({ commit }) {
            return new Promise((resolve) => {
                setTimeout(() => {
                    commit(SET_CURRENCY,
                        [
                            /**
                             * TODO Set data mockup from localStorage
                             */
                        ],
                    );
                    resolve();
                }, 500);
            });
        },
        [ADD_CURRENCY]({ commit, state }, {crypto, fiat}) {
            return fetch(`https://min-api.cryptocompare.com/data/price?fsym=${crypto}&tsyms=${fiat}&api_key=${process.env.API_CRYPTO_COMPARE}`)
                .then(response => response.json())
                .then(response => {
                    /**
                     * Наверное не правильно именно здесь выбрасывать исключение
                     */
                    if(
                        response["Response"] != "undefined"
                        && response["Response"] == "Error"
                    ) {
                        throw new Error(response["Message"]);
                    }
                    commit(SET_CURRENCY, [
                        ...state.collection, {
                            crypto,
                            fiat,
                            value: response[fiat],
                        },
                    ]);
                }).catch(error => {
                    console.log(error);
                });
        },
        // [DELETE_CURRENCY]
    };
}
